﻿using System;
using System.Collections;

namespace BloomFilter
{
    public interface IBloomFilter<T>
    {
        /// <summary>
        /// Adds a new item to the filter. It cannot be removed.
        /// </summary>
        void Add(T item);

        /// <summary>
        /// Checks for the existance of the item in the filter for a given probability.
        /// </summary>
        bool Contains(T item);

        /// <summary>
        /// The ratio of false to true bits in the filter. E.g., 1 true bit in a 10 bit filter means a truthiness of 0.1.
        /// </summary>
        double Truthiness { get; }
    }
    
    public class IntBloomFilter : BloomFilter<int>
    {
        /// <summary>
        /// Creates a new Bloom filter.
        /// </summary>
        /// <param name="capacity">The anticipated number of items to be added to the filter. More than this number of items can be added, but the error rate will exceed what is expected.</param>
        /// <param name="errorRate">The accepable false-positive rate (e.g., 0.01F = 1%)</param>
        /// <param name="m">The number of elements in the BitArray.</param>
        /// <param name="k">The number of hash functions to use.</param>
        public IntBloomFilter(int capacity, float? errorRate = null, int? m = null, int? k = null) : base(capacity, HashInt32, errorRate, m, k) { }

        /// <summary>
        /// Hashes a 32-bit signed int using Thomas Wang's method v3.1 (http://www.concentric.net/~Ttwang/tech/inthash.htm).
        /// Runtime is suggested to be 11 cycles. 
        /// </summary>
        /// <param name="input">The integer to hash.</param>
        /// <returns>The hashed result.</returns>
        protected static int HashInt32(int input)
        {
            uint x = (uint)input;
            unchecked
            {
                x = ~x + (x << 15); // x = (x << 15) - x- 1, as (~x) + y is equivalent to y - x - 1 in two's complement representation
                x = x ^ (x >> 12);
                x = x + (x << 2);
                x = x ^ (x >> 4);
                x = x * 2057; // x = (x + (x << 3)) + (x<< 11);
                x = x ^ (x >> 16);
                return (int)x;
            }
        }
    }
    
    public class StringBloomFilter : BloomFilter<string>
    {
        /// <summary>
        /// Creates a new Bloom filter.
        /// </summary>
        /// <param name="capacity">The anticipated number of items to be added to the filter. More than this number of items can be added, but the error rate will exceed what is expected.</param>
        /// <param name="errorRate">The accepable false-positive rate (e.g., 0.01F = 1%)</param>
        /// <param name="m">The number of elements in the BitArray.</param>
        /// <param name="k">The number of hash functions to use.</param>
        public StringBloomFilter(int capacity, float? errorRate = null, int? m = null, int? k = null) : base(capacity, HashString, errorRate, m, k) { }

        /// <summary>
        /// Hashes a string using Bob Jenkin's "One At A Time" method from Dr. Dobbs (http://burtleburtle.net/bob/hash/doobs.html).
        /// Runtime is suggested to be 9x+9, where x = input.Length. 
        /// </summary>
        /// <param name="input">The string to hash.</param>
        /// <returns>The hashed result.</returns>
        protected static int HashString(string input)
        {
            int hash = 0;

            for (int i = 0; i < input.Length; i++)
            {
                hash += input[i];
                hash += (hash << 10);
                hash ^= (hash >> 6);
            }
            hash += (hash << 3);
            hash ^= (hash >> 11);
            hash += (hash << 15);
            return hash;
        }
    }

    /// <summary>
    /// From https://gist.github.com/richardkundl/8300092 (check comments).
    /// </summary>
    public class BloomFilter<T> : IBloomFilter<T>
    {
        /// <summary>
        /// Creates a new Bloom filter.
        /// </summary>
        /// <param name="capacity">The anticipated number of items to be added to the filter. More than this number of items can be added, but the error rate will exceed what is expected.</param>
        /// <param name="errorRate">The accepable false-positive rate (e.g., 0.01F = 1%)</param>
        /// <param name="hashFunction">The function to hash the input values. Do not use GetHashCode().</param>
        /// <param name="m">The number of elements in the BitArray.</param>
        /// <param name="k">The number of hash functions to use.</param>
        public BloomFilter(int capacity, Func<T, int> hashFunction, float? errorRate = null, int? m = null, int? k = null)
        {
            // validate the params are in range
            if (capacity < 1)
            {
                throw new ArgumentOutOfRangeException("capacity", capacity, "capacity must be > 0");
            }

            errorRate = errorRate ?? BestErrorRate(capacity);

            if (errorRate >= 1 || errorRate <= 0)
            {
                throw new ArgumentOutOfRangeException("errorRate", errorRate, String.Format("errorRate must be between 0 and 1, exclusive. Was {0}", errorRate));
            }

            m = m ?? BestM(capacity, errorRate.Value);

            // from overflow in bestM calculation 
            if (m < 1)
            {
                throw new ArgumentOutOfRangeException(String.Format("The provided capacity and errorRate values would result in an array of length > int.MaxValue. Please reduce either of these values. Capacity: {0}, Error rate: {1}", capacity, errorRate));
            }

            getHashSecondary = hashFunction;

            hashFunctionCount = k ?? BestK(capacity, errorRate.Value);
            hashBits = new BitArray(m.Value);
        }

        /// <inheritdoc/>
        public void Add(T item)
        {
            // start flipping bits for each hash of item
            int primaryHash = item.GetHashCode();
            int secondaryHash = getHashSecondary(item);
            for (int i = 0; i < hashFunctionCount; i++)
            {
                int hash = ComputeHash(primaryHash, secondaryHash, i);
                hashBits[hash] = true;
            }
        }

        /// <inheritdoc/>
        public bool Contains(T item)
        {
            int primaryHash = item.GetHashCode();
            int secondaryHash = getHashSecondary(item);
            for (int i = 0; i < hashFunctionCount; i++)
            {
                int hash = ComputeHash(primaryHash, secondaryHash, i);
                if (hashBits[hash] == false)
                {
                    return false;
                }
            }
            return true;
        }

        /// <inheritdoc/>
        public double Truthiness
        {
            get
            {
                return (double)TrueBits() / hashBits.Length;
            }
        }

        private int TrueBits()
        {
            int output = 0;
            foreach (bool bit in hashBits)
            {
                if (bit == true)
                {
                    output++;
                }
            }
            return output;
        }

        /// <summary>
        /// Performs Dillinger and Manolios double hashing. 
        /// </summary>
        private int ComputeHash(int primaryHash, int secondaryHash, int i)
        {
            int resultingHash = (primaryHash + (i * secondaryHash)) % hashBits.Length;
            return Math.Abs(resultingHash);
        }

        private int hashFunctionCount;
        private BitArray hashBits;
        private Func<T, int> getHashSecondary;

        private static int BestK(int capacity, float errorRate)
        {
            return (int)Math.Round(Math.Log(2.0) * BestM(capacity, errorRate) / capacity);
        }

        private static int BestM(int capacity, float errorRate)
        {
            return (int)Math.Ceiling(capacity * Math.Log(errorRate, (1.0 / Math.Pow(2, Math.Log(2.0)))));
        }

        private static float BestErrorRate(int capacity)
        {
            float c = (float)(1.0 / capacity);
            if (c != 0)
            {
                return c;
            }
            else
            {
                return (float)Math.Pow(0.6185, int.MaxValue / capacity); // http://www.cs.princeton.edu/courses/archive/spring02/cs493/lec7.pdf
            }
        }
    }
}
